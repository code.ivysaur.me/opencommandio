# opencommandio

![](https://img.shields.io/badge/written%20in-Javascript%20%28node.js%29-blue)

A web-based parallel SSH console for managing multiple linux servers.

Do you have too many servers to manage manually, but not big enough for puppet/chef? Want an interactive feel while still deduplicating effort across 3-10 servers? Inspired by commando.io, this is a commando.io alternative with the following features:

## Features

- Add servers by password or keypair authentication
- Host the control server locally to avoid trusting a third party
- Arrange servers into groups (e.g. all web servers, all database servers, ...)
- Supports running commands in parallel
- Response streams back in realtime over websockets
- Run any #! script remotely (bash, php, node, python, ...)
- Syntax highlighting text editor (thanks Codemirror!)
- Includes some demo scripts demonstrating using multiple languages, realtime, and a longer script to update node.js binaries from nodejs.org

## Usage

- Install from npm (`npm install opencommandio`)
- Configure servers.js as per inline documentation
- Run the server (`node ./core.js`)
- Browse to localhost:8081 and enjoy
- Optional: Protect the server by nginx reverse proxy with http authentication, or similar

Tags: sysadmin

Npm page: https://npmjs.org/package/opencommandio
Inspired by: https://github.com/nodesocket/commando (unfortunately moved to a commercial closed-source SaaS)

Check npm to see if there's a more recent version than from here.


## Download

- [⬇️ opencommandio-1.1.1.tgz](dist-archive/opencommandio-1.1.1.tgz) *(148.86 KiB)*
- [⬇️ opencommandio-1.1.0.tgz](dist-archive/opencommandio-1.1.0.tgz) *(147.71 KiB)*
- [⬇️ opencommandio-1.0.0.tgz](dist-archive/opencommandio-1.0.0.tgz) *(146.97 KiB)*
